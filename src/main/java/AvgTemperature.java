import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;

public class AvgTemperature {

	public static void main(String args[]) throws IOException {
		JobConf conf = new JobConf(WordCount.class);
		conf.setJobName("wordcount");
		conf.set("fs.default.name", "hdfs://HNName:10001");
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(IntWritable.class);

		conf.setMapperClass(AvgTemperatureMapper.class);
		conf.setReducerClass(AvgTemperatureReducer.class);

		FileInputFormat.setInputPaths(conf,new Path("/data/large"));
		FileOutputFormat.setOutputPath(conf, new Path("/data/large/output"));
		
		JobClient.runJob(conf);
	}
}